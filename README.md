
# `ulog` - A radically minimalist Rust logger for `no_std` environments

`ulog` is a radically minimalist logger for Rust, compatible with the
[`log`](https://crates.io/crates/log) module. It is heavily inspired and based
on [`simplelog`](https://crates.io/crates/simplelog)'s `WriteLogger`
implementation, however it aims to run in `no_std` environments.

## License

Copyright 2021 Rob Connolly

Dual [MIT](LICENSE-MIT) and [Apache 2.0](LICENSE-APACHE) licenses. 
