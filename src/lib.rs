
#![feature(const_fn)]

use core::fmt::Write;
use spin::Mutex;
use log::*;

pub struct ULogger<W: Write + Send + Sync + 'static> {
    level: LevelFilter,
    writer: Mutex<W>,
}

impl <W: Write + Send + Sync + 'static> ULogger<W> {
    pub const fn new(level: LevelFilter, writer: W) -> ULogger<W> {
        ULogger::<W> {
            level: level,
            writer: Mutex::new(writer),
        }
    }
}

impl <W: Write + Send + Sync + 'static> Log for ULogger<W> {
    fn enabled(&self, metadata: &Metadata) -> bool {
        metadata.level() <= self.level
    }

    fn log(&self, record: &Record) {
        if self.enabled(record.metadata()) {
            let mut locked_writer = self.writer.lock();
            write!(locked_writer, "{}:{} -- {}",
                record.level(),
                record.target(),
                record.args()).unwrap();
        }
    }

    fn flush(&self) {}
}

#[cfg(test)]
mod tests {

    use log::*;
    use core::fmt::{Write, Error};
    use crate::ULogger;

    static mut RESULT : String = String::new();

    #[derive(Copy,Clone)]
    struct TestWriter {
        test_str: &'static str,
    }

    impl Write for TestWriter {
        fn write_str(&mut self, s: &str) -> Result<(), Error> {
            unsafe {
                RESULT.push_str(s);
            }
            Ok(())
        }
    }

    #[test]
    fn basic_log() {
        static BUF: TestWriter = TestWriter {
            test_str: "INFO:ulog::tests -- Hello World!",
        };
        static LOGGER: ULogger<TestWriter> = ULogger::<TestWriter>::new(
            LevelFilter::Info,
            BUF,
        );

        log::set_logger(&LOGGER)
            .map(|()| log::set_max_level(LevelFilter::Info)).unwrap();

        info!("Hello World!");

        unsafe {
            assert_eq!(BUF.test_str, RESULT);
            RESULT.clear();
        }
    }
}
